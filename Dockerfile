#################################################################
# venv for Flywheel (flypy)
##################################################################
FROM flywheel/python:3.12-debian as fw_base
ENV FLYWHEEL="/flywheel/v0"
WORKDIR ${FLYWHEEL}

# Dev install. git for pip editable install.
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get clean
RUN apt-get install --no-install-recommends -y \
    git \
    build-essential \
    zip \
    nodejs \
    tree && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN python -m venv /opt/flypy
ENV PATH="/opt/flypy/bin:$PATH"

# Installing main dependencies
COPY requirements.txt $FLYWHEEL/
RUN pip install -U --no-cache-dir pip 
RUN pip install --no-cache-dir -r $FLYWHEEL/requirements.txt

COPY ./ $FLYWHEEL/
RUN pip install --no-cache-dir .

# Isolate the versions of the dependencies within the BIDS App
# from the (potentially updated) Flywheel dependencies by copying
# the venv with the pip installed Flyhweel deps.

### 1.8.6 runs py 3.10.6 from /usr/share/fsl/6.0/bin/python
FROM fcpindi/c-pac:release-v1.8.7 as bids_runner
ENV FLYWHEEL="/flywheel/v0"
WORKDIR ${FLYWHEEL}

COPY --from=fw_base /opt/flypy /opt/flypy
COPY --from=fw_base /usr/local /usr/local
# Update the softlink to point to fw_base's version of python in bids_runner
#RUN ln -sf /usr/local/bin/python3.10 /opt/flypy/bin/python

# "Re-copy the gear's guts
# NOTE: The deps should be captured in /opt/flypy
COPY ./ $FLYWHEEL/

## Configure entrypoint
RUN chmod a+x $FLYWHEEL/run.py
ENTRYPOINT ["/opt/flypy/bin/python", "/flywheel/v0/run.py"]
