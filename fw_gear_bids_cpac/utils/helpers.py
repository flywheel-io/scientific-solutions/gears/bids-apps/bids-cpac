"""Small methods specific to this BIDS app gear."""

from flywheel_bids.flywheel_bids_app_toolkit import BIDSAppContext
from flywheel_bids.flywheel_bids_app_toolkit.commands import validate_kwargs


def validate_setup(app_context: BIDSAppContext):
    """Customizable validation pipeline for gear-dependent configuration options.

    The goal is to cause the gear to fail quickly and with useful debugging help
    prior to downloading BIDS data or running (doomed) BIDS algorithms.

    Validating the bids_app_options kwargs should be included for all BIDS App gears.
    Other items to consider for validation include:
    1) Do any input files require other input files?
    2) Do other modification scripts/methods need to be run on inputs?
    3) Are any config settings mutually exclusive and need to be double-checked?

    Args:
        app_context (BIDSAppContext): information to launch the BIDS App
    """
    if app_context.bids_app_options:
        validate_kwargs(app_context, alt_run_cmd=["python", "/code/run.py", "--help"])
