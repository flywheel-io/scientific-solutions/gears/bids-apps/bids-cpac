"""Parser module to parse gear config.json."""

import logging
from typing import Dict, Tuple

from flywheel_gear_toolkit import GearToolkitContext

log = logging.getLogger(__name__)


# This function mainly parses gear_context's config.json file and returns relevant
# inputs and options.
def parse_config(
    gear_context: GearToolkitContext,
) -> Tuple[bool, Dict]:
    """Search config for extra settings not used by BIDSAppContext.

    Args:
        gear_context (GearToolkitContext):

    Returns:
        debug (bool): Level of gear verbosity
        config_options (Dict): other config options explicitly called out
                in the manifest, not encapsulated by the bids_app_command
                field. The bids_app_command field is intended to handle the
                majority of the use cases to run the BIDS app.
    """
    debug = gear_context.config.get("debug")
    config_options = {}
    for key in gear_context.config.keys():
        # If there are extra kwargs that were highlighted in the Config UI
        # by putting them in the manifest specifically, add them to the list
        # to exclude (i.e., after "bids_app_command")
        if not key.startswith("gear-") and key not in ["debug", "bids_app_command"]:
            config_options[key] = gear_context.config.get(key)

    return debug, config_options


def parse_input_files(gear_context: GearToolkitContext):
    """Fetch the input files from the manifest and return the filepaths.

    Grab the manifest blob for inputs. Refine to only file inputs and create a
    key-value pair entry in the input_files dictionary. Best practice design will be
    to use the kwarg in the BIDS App command as the input file label (key).
    """
    inputs = gear_context.manifest.get("inputs")
    input_files = {}
    if inputs:
        for i in [k for k in inputs.keys() if k not in ["archived_runs"]]:
            if inputs[i]["base"] == "file" and gear_context.get_input_path(i):
                input_files[i] = gear_context.get_input_path(i)

    return input_files
