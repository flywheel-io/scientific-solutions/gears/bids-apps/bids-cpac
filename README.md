# BIDS-CPAC

# Updating the BIDS algorithm version

The preferred method is to edit and run .utils.autoupdate and .tests.platform_test (see
global vars at the top). The main phases of the autoupdate script are captured in the
steps below, which one may run manually. The platform_test is part of the script and
attempts to run the newly built gear with minimal inputs.

1. Fork the [repo](https://gitlab.com/flywheel-io/scientific-solutions/gears/bids-apps/bids-cpac).
2. [Update the DockerHub image](https://gitlab.com/flywheel-io/scientific-solutions/gears/bids-apps/bids-cpac/-/blob/main/Dockerfile?ref_type=heads#L37) that the gear will use.
3. Update the [gear-builder line](https://gitlab.com/flywheel-io/scientific-solutions/gears/bids-apps/bids-cpac/-/blob/main/manifest.json?ref_type=heads#L82) in the manifest.
4. Update the [version line](https://gitlab.com/flywheel-io/scientific-solutions/gears/bids-apps/bids-cpac/-/blob/main/manifest.json?ref_type=heads#L159) in the manifest.
5. Run `poetry update` from the local commandline (where your cwd is the top-level of the gear). This command will update any dependencies for the Flywheel portion of the gear (not the BIDS algorithm itself!).
6. Run `fw-beta gear build` to update anything in the manifest.
7. Ideally, run `fw-beta gear upload` and complete a test run on your Flywheel instance.
8. Run `git checkout -b {my-update}`, `git commit -a -m "{My message about updating}" -n`, and `git push`.
9. Submit a merge request (MR) to the original gear repo for Flywheel staff to review for official inclusion in the exchange.

## Overview

[Usage](#usage)

[FAQ](#faq)

### Summary

A configurable, open-source, Nipype-based, automated processing pipeline for resting
state fMRI data. Designed for use by both novice users and experts, C-PAC brings the
power, flexibility and elegance of Nipype to users in a plug-and-play fashion; no
programming required.

### Cite

Moving Beyond Processing and Analysis-Related Variation in Neuroscience
Xinhui Li, Nathalia Bianchini Esper, Lei Ai, Steve Giavasis, Hecheng Jin, Eric Feczko,
Ting Xu, Jon Clucas, Alexandre Franco, Anibal Solon Heinsfeld, Azeez Adebimpe, Joshua T.
Vogelstein, Chao-Gan Yan, Oscar Esteban, Russell A. Poldrack, Cameron Craddock, Damien
Fair, Theodore Satterthwaite, Gregory Kiar, Michael P. Milham
bioRxiv 2021.12.01.470790; doi: https://doi.org/10.1101/2021.12.01.470790

The C-PAC website is located here: <http://fcp-indi.github.io>

### License

LICENSING NOTE: FSL software are owned by Oxford University Innovation and license is
required for any commercial applications. For commercial licence please contact
fsl@innovation.ox.ac.uk. For academic use, an academic license is required which is
available by registering on the FSL website. Any use of the software requires that the
user obtain the appropriate license.
See https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/Licence for more information.

Gear: MIT
ANTS: Apache 2.0
AFNI: GNU GPL
FreeSurfer: GNU GPL + license file from MGH
FSL: see note above

### Classification

_Category:_ Analysis

_Gear Level:_

- [ ] Project
- [x] Subject
- [ ] Session
- [ ] Acquisition
- [ ] Analysis

---

[[_TOC_]]

---

### Overview

This gear downloads the [BIDS directory structure](https://bids.neuroimaging.io/) from
the associated flywheel session and processes it according to the specified parameters
and the preferred pipeline configuration (default or the optional pipeline_file input).

To run the gear with all defaults from C-PAC, no configuration is necessary. To run the
gear as one would from the commandline, enter the command in the `bids_app_command`
configuration box as you would when running C-PAC with a Docker container. You may wish
to copy and paste a standardized command into the configuration box.
For example, if you would like to run only the anatomical preprocessing,
enter `cpac bids_dir output_dir participant --preconfig anat-only`. Note that "bids_dir"
and "output_dir" are essentially placeholders and can be exactly those values. The gear
will replace the arguments with the proper Flywheel folders so that your results will be
available at the end of a successful gear run.

Please refer to the C-PAC documentation to determine the command options that are
appropriate for your analysis. Any keyword arguments requiring a file are mirrored on
the input tab. If an input file is given, the corresponding kwarg will be added to the
command. (i.e., if no command is entered in `bids_app_command`, any input files plus
their kwargs will be added to the default command; any input files plus corresponding
kwargs will also be added to any custom command given in the `bids_app_command` box.)
Entering the full command with the input file keyword arg and file in the command box is
also acceptable. The gear will automatically update the path to the file supplied in the
input tab appropriately.

For a supplied pipeline file, it is recommended to use
a [C-PAC configuration GUI](http://fcp-indi.github.io/docs/user/new_gui.html) to reduce
the potential for errors.

The size of the zipped gear output will depend on the C-PAC pipeline settings. For
example, the zipped output of the default pipeline can exceed 4 GB. However, using the
--anat-only flag will reduce this to under 70 MB.

### Inputs

- pipeline_file
  - OPTIONAL
  - **Type**: file
  - See https://fcp-indi.github.io/docs/latest/user/pipelines/pipeline_config for more
    information about constructing the yaml file used to specify pipeline options. By
    default, C-PAC (and this wrapper gear) will use the "default" pipeline and does
    not require this field to be populated.
- archived_runs
  - OPTIONAL
  - **Type**: file
  - If there are other files that will allow the algorithm to run or pick up analysis
    from a certain point, then the archive can be supplied here. This zip will be
    automatically unzipped within the gear. NOTE: When the BIDSAppContext is
    created, if this field is present (as "archived_runs"), the toolkit will unzip
    the attached file into the BIDS directory and update paths appropriately. See
    BIDSAppContext.check_archived_inputs for more details.

### Config

- bids_app_command

  - OPTIONAL
  - **Type**: free-text
  - The gear will run the defaults for the algorithm without anything in this box. If
    you wish to provide the command as you would on a CLI, input the exact command
    here. Flywheel will automatically update the BIDS_dir, output_dir, and
    analysis_level. (
    e.g., `cpac bids_dir output participant --valid-arg1 --valid-arg2`)
  - For more help to build the command, please see REPLACE:<BIDS App documentation>.
  - Note: If you use a kwarg here, don't worry about putting a value in the box for
    the same kwarg below. Any kwarg that you see called out in the config UI and that
    is given a value will supersede the kwarg:value given as part of the
    bids_app_command.

- debug

  - **Type**: Boolean
  - **Default**: false
  - Verbosity of log messages; default results in INFO level, True will log DEBUG
    level

- gear-dry-run

  - **Type**: Boolean
  - **Default**: false (set to true for template)
  - Do everything related to Flywheel except actually execute BIDS App command.
  - Note: This is NOT the same as running the BIDS app command with `--dry-run`.
    gear-dry-run will not actually download the BIDS data, attempt to run the BIDS app
    command, or do any metadata/result updating.

- gear-post-processing-only
  - **Type**: Boolean
  - **Default**: false
  - If an archive file is available, one can pick up with a previously analyzed
    dataset. This option is useful if the metadata changed (or needs to change). It is
    also useful for development.
- tracking_opt-out
  - **Type**: Boolean
  - **Default**: true
  - Turn off reporting the number of participants analyzed to C-PAC

### Outputs

#### Files

bids_tree.html

- Description of the available BIDS files and structure

bids-cpac*cpac*{destination_container}.zip

- Output files from the algorithm

cpac_log.txt

- Log from the C-PAC algorithm

job.log

- Details on the gear execution on Flywheel

#### Metadata

Any notes on metadata created by this gear

### Pre-requisites

This section contains any prerequisites

#### Prerequisite Gear Runs

BIDS-curate must run successfully in order for any BIDS app gear to be able to download
the proper data.

#### Prerequisite Files

None noted.

#### Prerequisite Metadata

A description of any metadata that is needed for the gear to run.
If possible, list as many specific metadata objects that are required:

1. BIDS
   - {container}.info.BIDS
   - Enables `export_bids` to download the proper data

## Usage

bids-cpac will run with very little intervention from the user. After selecting a
session and clicking "Run Gear", selections may be made from the GUI to change
default behavior or the user may start the gear without changing anything.

If the user wishes to provided pipeline details other than those that come with
C-PAC, the user may enter the pipeline yaml file in the Inputs field for
"pipeline_file".

Previous processing that C-PAC would normally look for in the BIDS directory may be
supplied in the Inputs tab under "archived_runs". This file will be unzipped into
the BIDS working directory.

In the Configuration tab, one may provide the commandline input you would run in a
terminal for C-PAC in the free-text bids_app_command field. The BIDS App Toolkit parses
any arguments beyond the typical `app_name BIDS_dir output_dir
analysis_level` when creating the BIDSAppContext object (see flywheel_bids.
flywheel_bids_app_toolkit.context.parse_bids_app_commands), so additional
commandline arguments will be passed to the algorithm. Additionally, the gear checks
the arguments against the C-PAC usage file early in the gear run to fail quickly, if
an errant argument is provided.

### Description

- The options from the GUI are turned into a BIDSAppContext, storing all the relevant
  options and files.
- IF the bids_app_command is populated, the command is parsed and checked against
  the C-PAC usage file. Extraneous or errant args will cause immediate gear failure.
- Flywheel details are collected for provenance and naming.
- A shell command is generated and cleaned, then passed to C-PAC.
- Results are parsed and updated as necessary to be zipped inside of
  /flywheel/v0/output upon conclusion of the gear run.
- The gear status is reported.

#### File Specifications

This section contains specifications on any input files that the gear may need

### Workflow

```mermaid
graph LR;
    A[Input-File]:::input --> C;
    C[Upload] --> D[Parent Container <br> Project, Subject, etc];
    D:::container --> E((Gear));
    E:::gear --> F[Analysis]:::container;

    classDef container fill:#57d,color:#fff
    classDef input fill:#7a9,color:#fff
    classDef gear fill:#659,color:#fff

```

Description of workflow

1. Upload file to container
1. Select file as input to gear
1. Gear places output in Analysis

### Logging

Debug is the highest number of messages and will report some locations/options and all
errors. If unchecked in the configuration tab, only INFO and higher priority level log
messages will be reported.

## FAQ

[FAQ.md](FAQ.md)

## Contributing

[For more information about how to get started contributing to that gear,
checkout [CONTRIBUTING.md](CONTRIBUTING.md).]

<!-- markdownlint-disable-file -->
