# Summaries

## 0.4.4_1.8.7

- ENH: Add HPC compatibility

## 0.4.0_1.8.6

- Having issues getting the algorithm to run, b/c the CLI `cpac run` tries to find a
  local Docker image. Others note trouble pulling and running [here](https://github.com/GT-EmoryMINDlab/preproc-docs?tab=readme-ov-file#5-install-c-pac)
- Use the image's python and /code/run.py to enter the args directly without the
  `cpac run` wrapper
