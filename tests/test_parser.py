"""Module to test parser.py"""

import pytest

from fw_gear_bids_cpac.parser import parse_config, parse_input_files


@pytest.mark.parametrize(
    "mock_config_dict, expected_debug, expected_config_options",
    [
        ({}, None, {}),
        ({"debug": "DEBUG"}, "DEBUG", {}),
        ({"debug": "ERROR", "my_opt": "is_boring"}, "ERROR", {"my_opt": "is_boring"}),
        (
            {"my_opt_2": "is_educated", "gear-special": "wd-40"},
            None,
            {"my_opt_2": "is_educated"},
        ),
    ],
)
def test_parse_config(
    mock_config_dict, expected_debug, expected_config_options, mock_context
):
    mock_context.config.keys.side_effect = [mock_config_dict.keys()]
    mock_context.config.get.side_effect = lambda key: mock_config_dict.get(key)
    debug, config_options = parse_config(mock_context)
    assert debug == expected_debug
    assert config_options == expected_config_options


def test_parse_input_files(mock_context):
    mock_context.manifest = {
        "inputs": {
            "file1": {"base": "file"},
            "non_file1": {"base": "not_file"},
            "file2": {"base": "file"},
        }
    }
    mock_context.get_input_path = lambda x: f"path/{x}"

    # Call the function with the mock GearToolkitContext object
    result = parse_input_files(mock_context)

    # Assert that the function returned the expected dictionary
    assert result == {"file1": "path/file1", "file2": "path/file2"}
